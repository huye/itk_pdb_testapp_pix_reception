from core.MultiApp import App

smalls={
    'git':"https://gitlab.cern.ch/wraight/itk_pdb_testapp",
    'other':"otherstuff"
}

myapp = App("itkTestApp", "Streamlit ITk PDB Test App", smalls)

myapp.main()

import numpy as np


def scaleF(Tbare, Tmod):
    Eg = 1.22
    k = 8.617333262e-5

    def kelvin(T):
        return 273.15 + T

    return (
        np.power(kelvin(Tmod) / kelvin(Tbare), 2)
        * np.exp(Eg * (Tmod - Tbare) / 2 / k / kelvin(Tmod) / kelvin(Tbare))
        - 1
    )


# Finding Leakage current and breakdown voltage
def calVbdIlc(serialNum, Vdepl, xdata, ydata):
    Vbd = 0
    Ilc = 0

    # Checking if the component is 3D by using YY-identifiers
    is3D = False
    if any(serialNum[6] == x for x in ["G", "H", "I", "J", "V", "W", "0", "1"]):
        is3D = True

    # 3D sensor criterias
    if is3D:
        if Vdepl == 0:
            Vdepl = 5
        I_voltage_point = Vdepl + 20
        # break_threshold = Vdepl + 20
        # I_treshold = 2.5  # current in uA

    # Planar sensor criterias
    else:
        if Vdepl == 0:
            Vdepl = 50
        I_voltage_point = Vdepl + 50
        # break_threshold = Vdepl + 70
        # I_treshold = 0.75  # current in uA

    # Finding leakage current at threshold voltage
    for idx, V in enumerate(xdata):
        if V < Vdepl:
            continue
        elif V <= I_voltage_point:
            # Vlc = V
            Ilc = ydata[idx]

        # Finding breakdown voltage for 3D
        if is3D:
            if ydata[idx] > ydata[idx - 5] * 2 and xdata[idx - 5] > Vdepl:
                Vbd = xdata[idx - 5]
                break

        # Finding breakdown voltage for Planar
        else:
            if ydata[idx] > ydata[idx - 1] * 1.2 and xdata[idx - 1] != 0:
                Vbd = V
                break
    if Vbd == 0:
        Vbd = ">%s" % round(xdata[-1], 1)
    return Vbd, Ilc

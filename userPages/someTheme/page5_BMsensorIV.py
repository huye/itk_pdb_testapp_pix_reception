# standard
import json
import os
import sys
from io import BytesIO

import itkdb.exceptions as itkX
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import streamlit as st
from plotly.subplots import make_subplots

import core.DBaccess as DBaccess
from core.Page import Page

# custom
# PDB stuff
sys.path.insert(0, "./userPages/someTheme/")
from analysis import scaleF, calVbdIlc  # noqa: E402


infoList = [
    "  * upload vendor _xlsx_ data file",
    "  * review test schema",
    "   * reset if required",
    "   * edit if required",
    "  * upload test schema",
    "   * delete test upload if required",
]

# main part


class Page5(Page):
    def __init__(self):
        super().__init__(
            "Bare Module IV Tests Upload", ":microscope: Upload IV Test", infoList
        )

    def main(self):
        super().main()

        # getting attribute
        pageDict = st.session_state[self.name]

        # check requirements to do stuff
        doWork = False
        try:
            if st.session_state.myClient:
                doWork = True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        # gatekeeping
        if not doWork:
            st.stop()

        # drag and drop method
        pageDict["file"] = st.file_uploader("Upload data file", type=["json"])
        if st.session_state.debug:
            st.write(pageDict["file"])

        if pageDict["file"] is None:
            st.write("No data file set")
            filePath = os.path.realpath(__file__)
            exampleFileName = "BareModule_sensorIV.json"
            if st.session_state.debug:
                st.write("looking in:", filePath[: filePath.rfind("/")])
                st.write(os.listdir(filePath[: filePath.rfind("/")]))
            # load json file stored in the BytesIO object
            with open(filePath[: filePath.rfind("/")] + "/" + exampleFileName) as f:
                input_json = json.load(f)
                output = BytesIO()
                output.write(json.dumps(input_json).encode())
            st.download_button(
                label="Download example", data=output, file_name=exampleFileName
            )

            # # input data online
            # st.write("## Or input data")
            #
            # infra.TextBox(pageDict,'component',"Enter component *:")
            # infra.TextBox(pageDict,'date',"Enter date:")
            # infra.TextBox(pageDict,'passed',"passed or not (enter 1 or 0) *:")
            # infra.TextBox(pageDict,'problems',"problems?:")

            # pageDict['results'] = {}
            # st.write('Results:')
            # infra.TextBox(pageDict['results'],'VOLTAGE',"Voltage list *:")
            # infra.TextBox(pageDict['results'],'CURRENT',"Current list *:")
            # infra.TextBox(pageDict['results'],'CURRENT_MEAN',"Current_mean list:")
            # infra.TextBox(pageDict['results'],'CURRENT_SIGMA',"Current_sigma list:")
            # infra.TextBox(pageDict['results'],'HUMIDITY',"Humidity list:")
            # infra.TextBox(pageDict['results'],'TEMPERSTURE',"Temperature list:")
            # infra.TextBox(pageDict['results'],'TIME',"Time list:")

            st.stop()

        input_data = json.load(pageDict["file"])

        st.write("# Read Data")

        # if not 'testType' in input_data.keys():
        #     input_data['testType'] = "SENSOR_IV"

        # input
        st.write("Component (serial number):", input_data["component"])
        st.write("Test Type:", input_data["testType"])
        st.write("date:", input_data["date"])
        st.write("passed:", input_data["passed"])
        st.write("problems:", input_data["problems"])

        results = input_data["results"]
        plotting = len(results["VOLTAGE"]) == len(results["CURRENT"])

        if not plotting:
            st.write(
                "Different length of voltage and current array, no attempt to plot IV curve."
            )
        else:

            # calculate break down voltage and leakage current. TODO: extract the depletion voltage from sensor wafer.
            Vbd, Ilc = calVbdIlc(
                input_data["component"], 0, results["VOLTAGE"], results["CURRENT"]
            )
            st.write("{:30} {}".format("Beak down voltage [V]:", Vbd))
            st.write("{:30} {}".format("Leakage current [μA]:", Ilc))

            #  put data in dataframe and scale
            df = pd.DataFrame(
                columns=[
                    "voltage",
                    "current",
                    "current_err",
                    "current_scaled",
                    "current_scaled_err",
                    "temperature",
                    "humidity",
                ]
            )

            has_temperature = len(results["TEMPERATURE"]) > 0
            scale = 1
            temperature = 20
            humidity = 0
            current_err = 0
            for i in range(len(results["VOLTAGE"])):
                if len(results["VOLTAGE"]) == len(results["TEMPERATURE"]):
                    temperature = results["TEMPERATURE"][i]
                elif has_temperature:
                    temperature = np.nanmean(results["TEMPERATURE"])
                scale = scaleF(temperature, 20) + 1

                if len(results["VOLTAGE"]) == len(results["HUMIDITY"]):
                    humidity = results["HUMIDITY"][i]
                elif len(results["HUMIDITY"]) > 0:
                    humidity = np.nanmean(results["HUMIDITY"])

                if len(results["VOLTAGE"]) == len(results["CURRENT_SIGMA"]):
                    current_err = results["CURRENT_SIGMA"][i]

                df.loc[i] = (
                    results["VOLTAGE"][i],
                    results["CURRENT"][i],
                    current_err,
                    scale * results["CURRENT"][i],
                    scale * current_err,
                    temperature,
                    humidity,
                )

            # figs = go.Figure()
            figs = make_subplots(specs=[[{"secondary_y": True}]])
            figs.update_layout(
                title_text="%s IV" % input_data["component"],
                legend=dict(
                    yanchor="bottom", y=1.02, xanchor="right", x=1, orientation="h"
                ),
            )
            figs.update_xaxes(title="voltage [V]")
            figs.update_yaxes(title="current [μA]", secondary_y=False)
            figs.update_yaxes(title="temperature [°C]", secondary_y=True)

            figs.add_trace(
                go.Scatter(
                    x=df["voltage"].values,
                    y=df["current"].values,
                    error_y=dict(symmetric=True, array=df["current_err"].values),
                    mode="lines+markers",
                    name="IV",
                ),
                secondary_y=False,
            )

            # plot temperature and corrected IV
            if has_temperature:
                figs.add_trace(
                    go.Scatter(
                        x=df["voltage"].values,
                        y=df["current_scaled"].values,
                        error_y=dict(
                            symmetric=True, array=df["current_scaled_err"].values
                        ),
                        mode="lines+markers",
                        name="IV corrected to 20°C",
                    ),
                    secondary_y=False,
                )

                figs.add_trace(
                    go.Scatter(
                        x=df["voltage"].values,
                        y=df["temperature"].values,
                        yaxis="y2",
                        mode="lines+markers",
                        name="Temperature",
                    ),
                    secondary_y=True,
                )

            if not isinstance(Vbd, str):
                #    figs.add_vline(x=Vbd, line_width=3, line_color='orange')
                figs.add_trace(
                    go.Scatter(
                        x=[Vbd, Vbd],
                        y=[
                            np.minimum(df["current_scaled"].min(), df["current"].min()),
                            np.maximum(df["current_scaled"].max(), df["current"].max()),
                        ],
                        mode="lines",
                        line={"color": "rgb(255,6,6)", "width": 3},
                        name="break down voltage",
                    ),
                    secondary_y=False,
                )

            st.plotly_chart(figs)

            img_bytes = figs.to_image(format="png")

        # upload test
        if st.button("Upload Test"):
            try:
                pageDict["upVal"] = DBaccess.DbPost(
                    st.session_state.myClient, "uploadTestRunResults", input_data
                )
                st.write(
                    "### **Successful Upload**:",
                    pageDict["upVal"]["componentTestRun"]["date"],
                )
                st.write(pageDict["upVal"])

                if plotting and pageDict["upVal"]["testRun"]["id"]:
                    # st.session_state.myClient.post("createBinaryTestRunParameter",
                    #        data=dict(testRun=pageDict['upVal']['testRun']['id'],parameter="IV_IMG"), # wrong parameter
                    #        files=dict(data=img_bytes))
                    st.session_state.myClient.post(
                        "createTestRunAttachment",
                        data=dict(
                            testRun=pageDict["upVal"]["testRun"]["id"],
                            title="%s_IV" % input_data["component"],
                            type="file",
                        ),
                        files=dict(data=img_bytes),
                    )
                    st.write("### **Successful Upload IV_IMG**")
            except itkX.BadRequest as b:
                st.write("### :no_entry_sign: Update **Unsuccessful**")
                st.write(b)

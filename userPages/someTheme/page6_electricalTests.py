# standard
import json

import numpy as np
import pandas as pd
import streamlit as st
from matplotlib import cm
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap

import core.DBaccess as DBaccess
from core.Page import Page

# custom
# PDB stuff

infoList = [
    "  * upload vendor _xlsx_ data file",
    "  * review test schema",
    "   * reset if required",
    "   * edit if required",
    "  * upload test schema",
    "   * delete test upload if required",
]


def nrows_and_ncolumns(chipType):
    if chipType == "RD53A":
        nrows = 192
        ncolumns = 400
    elif chipType == "RD53B":
        nrows = 384
        ncolumns = 400

    return nrows, ncolumns


def plot2d(data, nrows, ncolumns, title, vmin, vmax):
    fig, ax = plt.subplots()
    ax.set_xlim([0, ncolumns])
    ax.set_ylim([0, nrows])
    ax.set_xlabel("Column")
    ax.set_ylabel("Row")
    ax.set_title(title)

    cmap = cm.get_cmap("turbo", 256)
    newcmap = ListedColormap(cmap(np.linspace(0.15, 0.85, 256)))

    im = ax.imshow(
        data,
        interpolation="none",
        cmap=newcmap,
        origin="lower",
        aspect="auto",
        vmin=vmin,
        vmax=vmax,
    )
    cbar = plt.colorbar(im)
    st.pyplot(fig)


def plotOcuupancyMap(occMap_json, sn, chipType):
    data = np.transpose(np.array(occMap_json["Data"]))

    nrows, ncolumns = nrows_and_ncolumns(chipType)
    plot2d(
        data, nrows, ncolumns, sn + "_OccupancyMap", 0, 200
    )  # np.percentile(data, 99))


def plotMask(maskMap_json, sn, chipType):
    data = np.transpose(np.array(maskMap_json["Data"]))

    nrows, ncolumns = nrows_and_ncolumns(chipType)
    plot2d(data, nrows, ncolumns, sn + "_EnMask", 0, 1)


def statOccupancyMap(occMap_json, target):
    return np.count_nonzero(np.array(occMap_json["Data"]) != target)


def statMask(maskMap_json):
    return np.count_nonzero(np.array(maskMap_json["Data"]) != 1)


# main part


class Page6(Page):
    def __init__(self):
        super().__init__(
            "Electrical Tests Upload", ":microscope: Upload Electrical Test", infoList
        )

    def main(self):
        super().main()

        # getting attribute
        pageDict = st.session_state[self.name]

        # check requirements to do stuff
        doWork = False
        try:
            if st.session_state.myClient:
                doWork = True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        # gatekeeping
        if not doWork:
            st.stop()

        st.subheader("Reference")
        st.write(
            "RD53A Module QC document on [EDMS](https://edms.cern.ch/ui/#!master/navigator/document?D:100967005:100967005:subDocs) "
        )

        with st.expander("Show electrical tests in Module QC flow"):
            st.markdown(
                "- First power up\n"
                "- Sensor IV \n"
                "- SLDO Qualification\n"
                "- Chip Configuration\n"
                "- ADC Calibration\n"
                "- Pixel Failure Test (Tuning)\n"
                "   - Digital scan\n"
                "   - Analog scan\n"
                "   - Threshold tuning\n"
                "   - ToT tuning\n"
                "   - Crosstalk scan\n"
                "   - Disconnected bump scan\n"
                "- Bump Bond Quality\n"
                "   - source scan\n"
                "- Module Selection"
            )

        st.subheader("Upload electrical tests")
        # Input component
        component_id = st.text_input("Module Serial Number:", value="")
        pageDict["component_id"] = component_id

        if component_id == "":
            st.stop()

        module = DBaccess.DbGet(
            st.session_state.myClient, "getComponent", {"component": component_id}
        )
        if module["componentType"]["code"] != "MODULE":
            st.write("Input is not a module!")
            st.stop()

        FEs = {}
        for c in module["children"]:
            if c["componentType"]["code"] == "BARE_MODULE":
                bm = DBaccess.DbGet(
                    st.session_state.myClient,
                    "getComponent",
                    {"component": c["component"]["serialNumber"]},
                )
                for k in bm["children"]:
                    if k["componentType"]["code"] == "FE_CHIP":
                        FEs[k["order"]] = k["component"]["serialNumber"]

        pageDict["FEs"] = FEs
        pageDict["type"] = module["type"]["code"]

        st.write("*{:30}* {}".format("Component type:", pageDict["type"]))
        st.write("*{:30}* {}".format("FEs:", ", ".join(FEs.values())))

        files_list = st.file_uploader(
            "Select all files in scan folder",
            accept_multiple_files=True,
            type=["json", "before", "after"],
        )

        files_dict = {f.name: f for f in files_list}
        if files_dict:
            st.write(files_dict.keys())

        if "scanLog.json" in files_dict:
            scanlog = json.load(files_dict["scanLog.json"])

            pageDict["testType"] = scanlog["testType"]
            pageDict["runNumber"] = scanlog["runNumber"]
            pageDict["timestamp"] = scanlog["timestamp"]

            st.write("{:30} {}".format("testType:", scanlog["testType"]))
            st.write("{:30} {}".format("runNumber:", scanlog["runNumber"]))
            st.write("{:30} {}".format("timestamp:", scanlog["timestamp"]))

            stats = pd.DataFrame()

            checkOccMask = False
            if any(
                x in scanlog["testType"]
                for x in ["_digitalscan", "_analogscan", "_discbumpscan"]
            ):  # TODO
                checkOccMask = True
                stats.insert(
                    loc=0,
                    column="chips",
                    value=["failed pixels in OccMap", "masked pixels"],
                )

            for connectivity in scanlog["connectivity"]:
                st.write("{:30} {}".format("chipType:", connectivity["chipType"]))
                st.write("%s chips in connectivity" % len(connectivity["chips"]))

                nrows, ncolumns = nrows_and_ncolumns(connectivity["chipType"])

                for i, chip in enumerate(connectivity["chips"]):
                    chipConfig = chip["config"].rsplit("/")[-1]

                    chip_id = None
                    if "serialNumber" in chip:
                        if chip["serialNumber"] in pageDict["FEs"].values():
                            chip_id = list(pageDict["FEs"].keys())[
                                list(pageDict["FEs"].values()).index(
                                    chip["serialNumber"]
                                )
                            ]
                        else:
                            st.write("chip %s not in module" % chip["serialNumber"])
                            break
                    else:
                        if chip["enable"] == 1:
                            config = json.load(files_dict[chipConfig + ".after"])
                            chip_id = config[connectivity["chipType"]]["Parameter"][
                                "ChipId"
                            ]
                            chip_name = config[connectivity["chipType"]]["Parameter"][
                                "Name"
                            ]

                            # for quad module
                            if len(pageDict["FEs"]) == 4:
                                if connectivity["chipType"] == "RD53A":
                                    chip_id -= 1
                                elif connectivity["chipType"] == "RD53B":
                                    chip_id -= 12

                            if (
                                chip_name in pageDict["FEs"].values()
                                and chip_id
                                != list(pageDict["FEs"].keys())[
                                    list(pageDict["FEs"].values()).index(chip_name)
                                ]
                            ):
                                st.write(
                                    "WARNING: chipID in config (%s) is not consist with PDB record (%s)"
                                    % (
                                        chip_id,
                                        list(pageDict["FEs"].keys())[
                                            list(pageDict["FEs"].values()).index(
                                                chip_name
                                            )
                                        ],
                                    )
                                )

                    # initialize the stats dataframe
                    stats.insert(loc=i + 1, column=FEs[chip_id], value=[np.nan, np.nan])

                    if chip["enable"] == 1:
                        st.write(
                            "*Chip {}*, sn:{}, enable: {}".format(
                                chip_id, FEs[chip_id], chip["enable"]
                            )
                        )
                        st.write(
                            "config:{:30}, uploaded? {}".format(
                                chipConfig + ".after",
                                chipConfig + ".after" in files_dict,
                            )
                        )
                        if checkOccMask:
                            occMap = chip_name + "_OccupancyMap"
                            st.write(
                                "\t {:30} {}".format(
                                    "OccupancyMap.json", occMap + ".json" in files_dict
                                )
                            )

                            occMap_json = json.load(files_dict[occMap + ".json"])

                            badPixels = statOccupancyMap(occMap_json, 100)
                            stats.at[0, FEs[chip_id]] = badPixels / (ncolumns * nrows)
                            st.write("non-100 occupancy pixels: ", badPixels)

                            l1Dist = chip_name + "_L1Dist"
                            st.write(
                                "\t {:30} {}".format(
                                    "L1Dist.json", l1Dist + ".json" in files_dict
                                )
                            )
                            enMask = chip_name + "_EnMask"
                            st.write(
                                "\t {:30} {}".format(
                                    "EnMask.json", enMask + ".json" in files_dict
                                )
                            )

                            maskMap_json = json.load(files_dict[enMask + ".json"])

                            maskedPixels = statMask(maskMap_json)
                            stats.at[1, FEs[chip_id]] = maskedPixels / (
                                ncolumns * nrows
                            )
                            st.write("masked pixels: ", maskedPixels)

                            col1, col2 = st.columns(2)
                            # plotting
                            with col1:
                                plotOcuupancyMap(
                                    occMap_json,
                                    FEs[chip_id],
                                    connectivity["chipType"],
                                )
                            with col2:
                                plotMask(
                                    maskMap_json,
                                    FEs[chip_id],
                                    connectivity["chipType"],
                                )

                    else:
                        if chip_id:
                            st.write(
                                "Chip {}, enable: {}, config:{:30}".format(
                                    chip_id, chip["enable"], chipConfig
                                )
                            )

            # show the statistics of the scan, grade the module
            st.subheader("Statistics of the scan")
            stats.set_index("chips")
            st.dataframe(stats)

            # TODO
            if st.button("Upload Test"):
                st.write("will be implemented!")

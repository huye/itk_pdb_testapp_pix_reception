# standard
from datetime import datetime
from datetime import timezone

import itkdb.exceptions as itkX
import pandas as pd
import streamlit as st

import core.DBaccess as DBaccess
import core.stInfrastructure as infra
from core.Page import Page


infoList = [
    "  * upload vendor _xlsx_ data file",
    "  * review test schema",
    "   * reset if required",
    "   * edit if required",
    "  * upload test schema",
    "   * delete test upload if required",
]

# main part


class Page9(Page):
    def __init__(self):
        super().__init__(
            "List Components by Stages",
            ":microscope: List Components by Stages",
            infoList,
        )

    def main(self):
        super().main()

        # getting attribute
        pageDict = st.session_state[self.name]

        # check requirements to do stuff
        doWork = False
        try:
            if st.session_state.myClient:
                doWork = True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        # gatekeeping
        if not doWork:
            st.stop()

        pageDict["institution"] = None
        pageDict["compList"] = None
        pageDict["componentType"] = None
        pageDict["type"] = None

        pageDict["institution"] = st.selectbox(
            "Institution",
            # sorted(i["code"] for i in st.session_state.Authenticate["instList"])
            (i["code"] for i in st.session_state.Authenticate["user"]["institutions"]),
        )

        pageDict["compList"] = DBaccess.DbGet(
            st.session_state.myClient,
            "listComponents",
            {
                "currentLocation": pageDict["institution"],
                "project": st.session_state.Authenticate["proj"]["code"],
            },
            True,
        )

        # if st.button('extract component type'):
        pageDict["componentType"] = st.selectbox(
            "Component Type",
            set([i["componentType"]["code"] for i in pageDict["compList"]]),
        )

        pageDict["type"] = st.selectbox(
            "Type",
            set(
                [
                    i["type"]["code"]
                    for i in pageDict["compList"]
                    if i["componentType"]["code"] == pageDict["componentType"]
                    and i["type"]
                ]
            ),
        )

        pageDict["stage"] = st.selectbox(
            "Stage",
            set(
                [
                    i["currentStage"]["code"]
                    for i in pageDict["compList"]
                    if i["componentType"]["code"] == pageDict["componentType"]
                    and i["type"]
                    and i["type"]["code"] == pageDict["type"]
                ]
            ),
        )

        df = pd.DataFrame(
            columns=[
                "Serial No.",
                "Component type",
                "Type",
                "Current Stage",
                "State",
                "Current location",
            ]
        )
        loc = 0

        for i in pageDict["compList"]:
            if i["type"]:
                df.loc[loc] = (
                    i["serialNumber"],
                    i["componentType"]["code"],
                    i["type"]["code"],
                    i["currentStage"]["code"],
                    i["state"],
                    i["currentLocation"]["name"],
                )
            else:
                df.loc[loc] = (
                    i["serialNumber"],
                    i["componentType"]["code"],
                    None,
                    i["currentStage"]["code"],
                    i["state"],
                    i["currentLocation"]["name"],
                )
            loc += 1

        df1 = df.loc[
            (df["Component type"] == pageDict["componentType"])
            & (df["Type"] == pageDict["type"])
            & (df["Current Stage"] == pageDict["stage"])
        ]

        st.write("%i components found" % (df1.index.size))
        st.dataframe(df1)

        tests = {}

        for sn in df1["Serial No."]:
            if sn is None:
                continue

            tests[sn] = pd.DataFrame(
                columns=["test", "runID", "run No.", "passed", "date"]
            )
            loc = 0
            component = DBaccess.DbGet(
                st.session_state.myClient, "getComponent", {"component": sn}
            )
            if component["tests"]:
                for t in component["tests"]:
                    for r in t["testRuns"]:
                        if r["state"] == "ready":
                            tests[sn].loc[loc] = (
                                t["code"],
                                r["id"],
                                r["runNumber"],
                                r["passed"],
                                r["date"],
                            )
                            loc += 1

            st.write("%s has %i tests" % (sn, tests[sn].index.size))
            if tests[sn].index.size > 0:
                st.dataframe(tests[sn])

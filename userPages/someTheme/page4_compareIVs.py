import sys

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import streamlit as st

import core.DBaccess as DBaccess
from core.Page import Page

# custom
sys.path.insert(0, "./userPages/someTheme/")
from analysis import scaleF, calVbdIlc  # noqa: E402


family_tree = {  # component type, test code
    "MODULE": "SENSOR_IV",
    "BARE_MODULE": "SENSOR_IV",
    "SENSOR_TILE": "IV_MEASURE",
}


def extractResultsModule(run, testruns):
    testruns[run["id"]] = {
        "date": run["date"],
        "institution": run["institution"]["code"],
        "passed": "passed",
        "breakdown_voltage": 0.0,
        "leakage_current": 0.0,
    }
    if not run["passed"]:
        testruns[run["id"]]["passed"] = "failed"
    for i in run["results"]:
        if i["code"] == "CURRENT":
            testruns[run["id"]]["current"] = i["value"]
        if i["code"] == "VOLTAGE":
            testruns[run["id"]]["voltage"] = i["value"]
        if i["code"] == "TEMPERATURE":
            testruns[run["id"]]["temperature"] = i["value"]
        if i["code"] == "HUMIDITY":
            testruns[run["id"]]["humidity"] = i["value"]


def extractResultsSensor(run, testruns):
    testruns[run["id"]] = {
        "date": run["date"],
        "institution": run["institution"]["code"],
        "passed": "passed",
    }
    if not run["passed"]:
        testruns[run["id"]]["passed"] = "failed"
    for i in run["results"]:
        if i["code"] == "BREAKDOWN_VOLTAGE":
            testruns[run["id"]]["breakdown_voltage"] = i["value"]
        if i["code"] == "LEAK_CURRENT":
            testruns[run["id"]]["leakage_current"] = i["value"]
        if i["code"] == "IV_ARRAY":
            testruns[run["id"]]["voltage"] = i["value"]["voltage"]
            testruns[run["id"]]["current"] = i["value"]["current"]
            testruns[run["id"]]["humidity"] = (
                i["value"]["humidity"] if "humidity" in i["value"].keys() else []
            )
            testruns[run["id"]]["temperature"] = (
                i["value"]["temperature"] if "temperature" in i["value"].keys() else []
            )


infoList = [
    "  * check settings for testType",
    "  * get test schema",
    "  * review test schema",
    "   * reset if required",
    "   * edit if required",
    "  * upload test schema",
    "   * delete test upload if required",
]

# ####################
# ## main part
# ####################


class Page4(Page):
    def __init__(self):
        super().__init__("Compare IV curves", ":microscope: compareIVs", infoList)

    def main(self):
        super().main()

        # get page name
        pageName = __file__.split("_")[-1].replace(".py", "")
        # title and (optional) instructions
        st.title(":chart_with_upwards_trend: Compare IV curves")
        st.markdown(
            "compare IV measurement results through the ITk pixel module production and assembly."
        )

        state = st.session_state
        if state.debug:
            st.write("  * select *institution*")
            st.write("  * select *project*")
            st.write("  * get list of *componentTypes*")
            st.write("  * select *componentType*")
            st.write("  * select *component*")
            st.write("  * get list of *testTypes*")
            st.write("  * select *testType*")
            st.write("  * get list of *testRuns* of *testType*")
            st.write("  * select *testRuns*")
            st.write("  * display results (plot for arrays)")
            st.write("  * select variable for plotting if required")
        else:
            st.write(" * toggle debug for details")
        st.write("---")

        # debug check
        if state.debug:
            st.write("### Debug is on")

        # add page attrubute to state
        # st.write([i for i in state.__dict__.keys() if i[:1] != '_'])
        if pageName in [i for i in state.__dict__.keys() if i[:1] != "_"]:
            if state.debug:
                st.write("state." + pageName + " defined")
        else:
            state.__setattr__(pageName, {})
        # st.write([i for i in state.__dict__.keys() if i[:1] != '_'])

        # # getting attribute
        # pageDict = state[pageName]

        # check requirements to do stuff
        doWork = False
        try:
            if state.myClient:
                doWork = True
            if state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        # gatekeeping
        if not doWork:
            st.stop()

        st.write("### Which component to check")

        # Input component
        component_id = st.text_input("Serial Number:", value="")

        if component_id != "":

            # extract testruns

            # get all the sensor related components in the family. Starting from the given component,
            # go through the parents until the top, then go through the children until the bottom.
            family = {}

            component = DBaccess.DbGet(
                state.myClient, "getComponent", {"component": component_id}
            )
            if component["componentType"]["code"] in family_tree:
                family[component_id] = component["componentType"]["code"]

            component0 = component
            while component and component["parents"]:
                for idx, p in enumerate(component["parents"]):
                    if p["componentType"]["code"] in family_tree:
                        if p["component"]:
                            family[p["component"]["serialNumber"]] = p["componentType"][
                                "code"
                            ]
                            component = DBaccess.DbGet(
                                state.myClient,
                                "getComponent",
                                {"component": p["component"]["serialNumber"]},
                            )
                        elif p["history"]:
                            for h in p["history"]:
                                if h["action"] == "assembly":
                                    family[h["component"]["serialNumber"]] = p[
                                        "componentType"
                                    ]["code"]
                                    component = DBaccess.DbGet(
                                        state.myClient,
                                        "getComponent",
                                        {"component": h["component"]["serialNumber"]},
                                    )
                        break
                    elif idx == len(component["parents"]) - 1:
                        component = {}

            component = component0
            while component and component["children"]:
                for idx, c in enumerate(component["children"]):
                    if c["componentType"]["code"] in family_tree:
                        family[c["component"]["serialNumber"]] = c["componentType"][
                            "code"
                        ]
                        component = DBaccess.DbGet(
                            state.myClient,
                            "getComponent",
                            {"component": c["component"]["serialNumber"]},
                        )
                        break
                    elif idx == len(component["children"]) - 1:
                        component = {}

            # # check
            # for sn, ctype in family.items():
            #     st.write('SN %s, type %s'%(sn, ctype))

            # extract a list of 'ready' testruns of the specified test type
            testruns = {}
            for c_id, c_type in family.items():
                component = DBaccess.DbGet(
                    state.myClient, "getComponent", {"component": c_id}
                )
                runs = {}
                for c in component["tests"]:
                    for r in c["testRuns"]:
                        if r["state"] == "ready":
                            # st.write('testrun id %s, state %s'%(r['id'], r['state']))
                            run = DBaccess.DbGet(
                                state.myClient, "getTestRun", {"testRun": r["id"]}
                            )
                            # st.write(run)
                            if (
                                component["componentType"]["code"]
                                in ["BARE_MODULE", "MODULE"]
                                and run["testType"]["code"] == "SENSOR_IV"
                            ):
                                extractResultsModule(run, runs)
                            if (
                                component["componentType"]["code"] == "SENSOR_TILE"
                                and run["testType"]["code"] == "IV_MEASURE"
                            ):
                                extractResultsSensor(run, runs)

                if len(runs) > 0:
                    testruns[c_id] = {}
                    testruns[c_id]["componentType"] = c_type
                    testruns[c_id]["testType"] = family_tree[c_type]
                    testruns[c_id]["testruns"] = runs

            # # check
            # st.write(component_id)
            loc = 0
            componment_info = {"sn": [], "componentType": [], "testrun_toCal": []}
            df = pd.DataFrame(
                columns=[
                    "TestType",
                    "TestID",
                    "Temperature",
                    "Humidity",
                    "Breakdown Voltage",
                    "Leakage Current",
                ]
            )

            for sn, data in testruns.items():
                componment_info["sn"].append(sn)
                componment_info["componentType"].append(data["componentType"])

                for run, rundata in data["testruns"].items():
                    Vbd = rundata["breakdown_voltage"]
                    Ilc = rundata["leakage_current"]
                    if Vbd == 0 or Ilc == 0:
                        componment_info["testrun_toCal"].append(run)

                        # Vdepl = float(st.text_input('Depletion voltage:', value=''))
                        Vdepl = 0
                        # extract depletion voltage from wafer property
                        for sn, ctype in family.items():
                            if ctype == "SENSOR_TILE":
                                sensor_tile = DBaccess.DbGet(
                                    state.myClient, "getComponent", {"component": sn}
                                )
                                wafer_sn = None
                                for p in sensor_tile["parents"]:
                                    if p["componentType"]["code"] == "SENSOR_WAFER":
                                        if p["component"]:
                                            wafer_sn = p["component"]["serialNumber"]
                                        elif p["history"]:
                                            for h in p["history"]:
                                                if h["action"] == "assembly":
                                                    wafer_sn = h["component"][
                                                        "serialNumber"
                                                    ]
                                if wafer_sn is not None:
                                    wafer = DBaccess.DbGet(
                                        state.myClient,
                                        "getComponent",
                                        {"component": wafer_sn},
                                    )
                                    for r in wafer["properties"]:
                                        if (
                                            r["code"] == "DEPLETION_VOLTAGE"
                                            and r["value"]
                                            and 0 < r["value"] < 200
                                        ):
                                            Vdepl = r["value"]

                        try:
                            Vbd, Ilc = calVbdIlc(
                                sn, Vdepl, rundata["voltage"], rundata["current"]
                            )
                        except Exception as e:
                            st.write("Calculation failed due to  %s" % e)

                    df.loc[loc] = (
                        data["testType"],
                        run,
                        np.nanmean(rundata["temperature"]),
                        np.nanmean(rundata["humidity"]),
                        Vbd,
                        Ilc,
                    )
                    loc += 1

            if len(componment_info["testrun_toCal"]) > 0:
                st.write(
                    "testrun [%s]: zero value in DB for break-down voltage or leakage current, recalculate."
                    % ",".join(componment_info["testrun_toCal"]),
                    "Depletion voltage is extracted from Wafer property,",
                    "if getting zero value, assume depletion voltage of 5V for 3D sensor and 50V for planar sensor.",
                )
            st.dataframe(df)

            # plot IV run results

            if len(testruns.keys()) > 0:
                st.write("Ploting IV curves")

                df = pd.DataFrame(columns=["testrun", "voltage", "current"])
                loc = 0
                for c, tests in testruns.items():
                    for r, run in tests["testruns"].items():
                        # print("debug__ run ", run)
                        for i in range(1, len(run["voltage"])):
                            if not run["temperature"]:
                                df.loc[loc] = (
                                    "%s_%s_%s_%s_%s"
                                    % (
                                        tests["componentType"],
                                        c,
                                        run["institution"],
                                        run["date"],
                                        run["passed"],
                                    ),
                                    run["voltage"][i],
                                    run["current"][i],
                                )
                            else:
                                if len(run["temperature"]) == len(run["current"]):
                                    c_scaled = (
                                        scaleF(run["temperature"][i], 20) + 1
                                    ) * run["current"][i]
                                    # c_scaled = run['current'][i]
                                else:
                                    c_scaled = (
                                        scaleF(np.nanmean(run["temperature"]), 20) + 1
                                    ) * run["current"][i]
                                    # c_scaled = run['current'][i]
                                df.loc[loc] = (
                                    "%s_%s_%s_%s_%s_corrected_to_20°C"
                                    % (
                                        tests["componentType"],
                                        c,
                                        run["institution"],
                                        run["date"],
                                        run["passed"],
                                    ),
                                    run["voltage"][i],
                                    c_scaled,
                                )
                            loc += 1

                all_testruns = df.testrun.unique()

                combFigs = go.Figure()
                combFigs.update_layout(
                    legend=dict(
                        yanchor="bottom", y=1.02, xanchor="right", x=1, orientation="h"
                    )
                )
                combFigs.update_xaxes(title="voltage [V]")
                combFigs.update_yaxes(title="current [uA]")

                for run in all_testruns:
                    sample = df.loc[df["testrun"] == run]
                    # print("debug__ sample", sample)
                    subFig = go.Scatter(
                        x=sample["voltage"].values,
                        y=sample["current"].values,
                        mode="lines+markers",
                        name=run,
                    )
                    combFigs.add_trace(subFig)

                if st.checkbox("log scale"):
                    combFigs.update_yaxes(type="log")

                st.plotly_chart(combFigs)

            else:
                st.Write("No test runs found.")

# standard
from datetime import datetime
from datetime import timezone

import itkdb.exceptions as itkX
import streamlit as st

import core.DBaccess as DBaccess
import core.stInfrastructure as infra
from core.Page import Page


infoList = [
    "  * upload vendor _xlsx_ data file",
    "  * review test schema",
    "   * reset if required",
    "   * edit if required",
    "  * upload test schema",
    "   * delete test upload if required",
]

# main part


class Page5(Page):
    def __init__(self):
        super().__init__("Bare Module Mass", ":microscope: Bare Module Mass", infoList)

    def main(self):
        super().main()

        # getting attribute
        pageDict = st.session_state[self.name]

        # check requirements to do stuff
        doWork = False
        try:
            if st.session_state.myClient:
                doWork = True
            if st.session_state.debug:
                st.write(":white_check_mark: Got Token")
        except AttributeError:
            st.write("No token")

        # gatekeeping
        if not doWork:
            st.stop()

        st.write("(*) is required")

        infra.TextBox(pageDict, "component", "Component *:")
        pageDict["institution"] = st.selectbox(
            "Institution",
            (i["code"] for i in st.session_state.Authenticate["user"]["institutions"]),
        )
        infra.TextBox(pageDict, "date", "Date:")
        infra.TextBox(pageDict, "runNumber", "Run number:")
        infra.TextBox(pageDict, "mass", "Total weigth *:")
        infra.TextBox(pageDict, "accuracy", "Scale accuracy *:")

        pageDict["passed"] = st.selectbox("Passed?", ("true", "false"))
        pageDict["problems"] = st.selectbox("Problem?", ("false", "true"))
        # infra.TextBox(pageDict,'problem',"Problem?")

        if st.button("Upload Test"):
            # No upload if required input is empty
            all_required = all(
                [pageDict["component"], pageDict["mass"], pageDict["accuracy"]]
            )
            if not all_required:
                st.write("Not all required inputs are provided.")

            sn_check = (
                len(pageDict["component"]) == 14 and pageDict["component"][5] == "B"
            )

            if not sn_check:
                st.write(
                    "Component serial number format is incorrect or not Bare Module."
                )

            if not all([all_required, sn_check]):
                st.write("No upload!")
            else:
                input_data = {}
                input_data["component"] = pageDict["component"]
                input_data["testType"] = "MASS_MEASUREMENT"
                input_data["runNumber"] = (
                    pageDict["runNumber"] if pageDict["runNumber"] != "" else "..."
                )
                input_data["institution"] = pageDict["institution"]
                if pageDict["date"] == "":
                    input_data["date"] = (
                        datetime.now()
                        .replace(tzinfo=timezone.utc)
                        .strftime("%Y-%m-%dT%H:%M:%S.%fZ")
                    )
                else:
                    input_data["date"] = pageDict["date"]
                input_data["passed"] = pageDict["passed"]
                input_data["problems"] = pageDict["problems"]
                input_data["properties"] = {"SCALE_ACCURACY": pageDict["accuracy"]}
                input_data["results"] = {"MASS": pageDict["mass"]}

                # st.write(input_data)
                try:
                    pageDict["upVal"] = DBaccess.DbPost(
                        st.session_state.myClient, "uploadTestRunResults", input_data
                    )
                    st.write(
                        "### **Successful Upload**:",
                        pageDict["upVal"]["componentTestRun"]["date"],
                    )
                    st.write(pageDict["upVal"])
                except itkX.BadRequest as b:
                    st.write("### :no_entry_sign: Update **Unsuccessful**")
                    st.write(b)

st_annotated_text==1.0.1
itkdb==0.4.4
pandas==1.2.4
streamlit==1.24.0
plotly==5.15.0
xlrd==2.0.1
openpyxl==3.0.7
altair==4.1.0

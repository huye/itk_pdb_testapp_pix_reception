### A copy of the itk_pdb_testapp repository (https://gitlab.cern.ch/wraight/itk_pdb_testapp/-/tree/master), several pages added.

---

To use the Visual Inspection page, one needs to rename the ```userPages/someTheme/page8_VisualInspection.py.disabled``` to ```/someTheme/page8_VisualInspection.py```, clone the qc-helper/qc-vi package (https://gitlab.cern.ch/atlas-itk/sw/db/pixels/qc-viz-tools-dev/visualinspectionsoftware.git) and put in qc-helper-each_test/qc-vi/

---

# itk_pdb_testapp

Example of streamline webApp for ITk PDB interface.

### [Streamlit](https://www.streamlit.io) (python**3**) code for interfacing with ITk Production Database (PDB).

**NB** Powered by [itkdb](https://pypi.org/project/itkdb/)

Check requirements file for necessary libraries.

See [itk-web-apps README](https://gitlab.cern.ch/wraight/itk-web-apps/) for instructions on running via Docker.

---

## Overview

Add content to _userPages_ directory.

Example pages in _userPages/someTheme_ directory.

---

## Example ages

### Workflow
  * generate sankey plot for component stages and testTypes

### Test Data Upload
  * upload module test manually, based on PDB schema

### Inventory
  * generate inventory of components at institute

---

## Run Custom streamlit webApp

1. Run streamlit
> streamlit run mainApp.py

2. Open browser at ''localhost:8501''

Optional arguments:
  * set port: "--server.port=8501"
